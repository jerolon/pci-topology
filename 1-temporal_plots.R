library(dplyr)
library(magrittr)
library(ggplot2)
library(tidyr)
library(purrr)
library(gghighlight)
setwd(dirname(rstudioapi::getActiveDocumentContext()$path))
source('netdymFunctions.R')
source('plotting_functions.R')
library(cowplot)

networks_df <-readRDS("networks_df.RDS")

#Part and whole are duplicated because it refered to if the network was the whole neuromast
#or only the hair cells and its neighbours
dyn_net_df <- filter(networks_df, type == "part") %>%
  #unnest the dynamic data (time,coordinates, area, perimeter)
  unnest(.,dyn_data)

#Transforming units, frames to minutes
dyn_net_df <- dyn_net_df %>% mutate(minutes = frame * (10/3) , time_of_last_Inv = time_of_last_Inv *(10/3))
#Pixels to microns
dyn_net_df <- dyn_net_df %>% mutate(across(ends_with(c("_x", "_y", "perimeter", "_length")), ~ .x/10))
#Pixels to square microns
dyn_net_df <- dyn_net_df %>% mutate(across(ends_with("area"), ~ .x/100))
#Take the distance of each cell to the mean position of the progenitor, during ~ the last 10 minutess before division
dyn_net_df <- dyn_net_df %>% mutate(dist_emx2_to_prog = distance(emx2_x, prog_x, emx2_y, prog_y), dist_notch_to_prog = distance(notch_x, prog_x, notch_y, prog_y))


#For easier plotting
dyn_net_df$invS <- factor(dyn_net_df$inv, levels = c(TRUE, FALSE), labels = c("Inverting", "Non inverting"))


# Graph difference in Neighbours ------------------------------------------
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , y = diff_N_neigh, col = inv), time_of_last_Inv ) + 
  labs(y = "Notch - Emx2 # Neighbours", title = "Difference in the number of neighbours")

dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , y = notch_Num_neigh, col = inv), time_of_last_Inv ) + 
  labs(y = "Notch# Neighbours", title = "Number of notch of neighbours")

#Number of neighbours by emx2/notch And inversion
p <- dyn_net_df %>% filter(phenotype == "Wildtype") %>% select(minutes, emx2_Num_neigh, notch_Num_neigh, inv, nombre, time_of_last_Inv) %>%
  gather(key = "hair_cell", value = "Number_neigh", 2:3) %>% 
  mutate(hair_cell = recode(hair_cell, emx2_Num_neigh = "emx2", notch_Num_neigh = "notch")) %>%
  group_by(nombre) %>% 
  ggplot(aes(x = minutes , y = Number_neigh, group = interaction(nombre, hair_cell))) + 
  geom_line(aes(linetype = hair_cell, col = inv), alpha = 1/3) + 
  geom_smooth(aes(group = interaction(hair_cell, inv), linetype = hair_cell, col = inv), n = 100, span = 0.3) +
  geom_rug(aes(x = time_of_last_Inv ), size = 1, sides = "bt", length = unit(0.05, "npc"))
add_plot_style(p) + coord_cartesian(xlim = c(0,300)) +  labs(y = "Number of Neighbours", title = "Number of neighbours")


#Cumulative sum of the difference. Figure 3E
#collect the mean and sd of inversion times in a dataframe
invTime <- dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
    group_by(nombre) %>% summarise(invTime = unique(time_of_last_Inv)) %>% pull(invTime)
invTimedf <- data.frame(desv = sd(invTime, na.rm = T), media = mean(invTime, na.rm = T))



p <- dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = cumsum(diff_N_neigh)) %>% 
  ggplot(aes(x = minutes , y = cumdiff, col = inv)) + theme_bw(base_size = 20) +
  #a shading showing standard deviation of inversion times
  geom_line(aes(group = nombre), alpha = 1, size = 1) + 
  labs(x = "Time [min]", color = '', y = "Cumulative difference in number of Neighbours") +
  #geom_sm() +
  geom_vline(aes(xintercept = invTimedf$media), linetype = "dashed", size = 1) +
  #unitScale +
  scale_y_continuous(breaks = scales::breaks_width(20)) +
  scale_x_continuous(expand = c(0,0), limits = c(0,227)) +
  scale_colour_discrete(breaks= c("TRUE","FALSE"), labels = c("Inverting","Non Inv")) +
  annotate("rect", fill = "red", alpha = 0.25, 
           xmin = invTimedf$media - invTimedf$desv, xmax = invTimedf$media + invTimedf$desv,
           ymin = -Inf, ymax = Inf)+
  theme(
    plot.title = element_text(face = "bold"),
    #legend.background = element_rect(fill = "white", size = 3, colour = "white"),
    legend.background = element_blank(),
    legend.justification = c(1, 1),
    legend.position = c(1, 1),
    legend.key = element_blank(),
    legend.direction = "horizontal",
    axis.ticks = element_line(colour = "grey70", size = 0.2),
    panel.grid.major = element_line(colour = "grey70", size = 0.2),
    panel.grid.minor = element_blank()
    
  )
  #plot_comp_Inv(aes(x = minutes , y = cumdiff, col = inv), time_of_last_Inv , yticks = 20) + 





#absolute of the cummulative difference
dyn_net_df %>% filter(phenotype == "Wildtype") %>%
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(diff_N_neigh))) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = inv), NULL, yticks = 10) + 
  geom_vline(aes(xintercept = invTimedf$media), linetype = "dashed", size = 1) +
  annotate("rect", fill = "red", alpha = 0.25, 
           xmin = invTimedf$media - invTimedf$desv, xmax = invTimedf$media + invTimedf$desv,
           ymin = -Inf, ymax = Inf) +
  labs(y = "# Neighbours", title = "Absolute Cummulative Difference in the number of neighbours")

#Separated by inverting, non inverting
dyn_net_df %>% filter(phenotype == "Wildtype", inv == T) %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(diff_N_neigh))) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = nombre), time_of_last_Inv , yticks = 10) + 
  labs(y = "Notch - Emx2 # Neighbours", title = "Absolute Cummulative Difference in the number of neighbours")

dyn_net_df %>% filter(phenotype == "Wildtype", inv == F) %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(diff_N_neigh))) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = nombre), time_of_last_Inv , yticks = 10) + 
  labs(y = "Notch - Emx2 # Neighbours", title = "Absolute Cummulative Difference in the number of neighbours")

#usinf the sign of the differences
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , y = sign(diff_N_neigh), col = inv), time_of_last_Inv ) + 
  labs(y = "Sign of Notch - Emx2 # Neighbours", title = "Difference in the number of neighbours")

#cumulative sign difference
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  group_by(nombre) %>%
  mutate(cumdiff = cumsum(sign(diff_N_neigh))) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Sign # Neighbours", title = "Cummulative sign Difference in the number of neighbours")

dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  group_by(nombre) %>% 
  plot_comp_Inv(aes(x = minutes , y = total_Num_neigh, col = inv), time_of_last_Inv , yticks = 1) + 
  labs(y = " # Neighbours", title = "Total neighbours of the hair cell pair")  + 
  #all pairs were tracked for AT LEAST 227 min after division
  scale_x_continuous(expand = c(0,0), limits = c(-33,227))


#absolute of the cumulative sign difference
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(sign(diff_N_neigh)))) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Sign # Neighbours", title = "Absolute Cummulative Difference in the number of neighbours")

#Separating inverting from non inverting
#absolute of the cumulative difference
dyn_net_df %>% filter(phenotype == "Wildtype", inv == T) %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(sign(diff_N_neigh)))) %>%
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = nombre), time_of_last_Inv , yticks = 10) + 
  labs(y = "Sign # Neighbours", title = "Absolute Cummulative Difference in the number of neighbours", subtitle = "Inverting cells")

dyn_net_df %>% filter(phenotype == "Wildtype", inv == F) %>% 
  group_by(nombre) %>% 
  mutate(cumdiff = abs(cumsum(sign(diff_N_neigh)))) %>%
  plot_comp_Inv(aes(x = minutes , y = cumdiff, col = nombre), time_of_last_Inv , yticks = 10) + 
  labs(y = "Sign # Neighbours", title = "Absolute Cummulative Difference in the number of neighbours", subtitle = "Non Inverting cells")

p <- dyn_net_df %>% filter(phenotype == "Wildtype")  %>% mutate(mean_area = (notch_area + emx2_area) / 2) %>%
  plot_comp_Inv(aes(x = minutes , y = mean_area, col = inv), time_of_last_Inv , yticks = 1000) + 
  labs(y = "Area (Pixels)", title = "Mean cell area") +
  coord_cartesian(ylim = c(0,4000))

#Area by emx2/notch And inversion
#Neighbours by emx2/notch And inversion
p <- dyn_net_df %>% 
  select(minutes, emx2_Num_neigh, notch_Num_neigh, inv, nombre, time_of_last_Inv, invS) %>%
  gather(key = "emx2_expression", value = "NumberNeighbours", 2:3) %>% 
  mutate(emx2_expression = recode(emx2_expression, emx2_Num_neigh = "emx2", notch_Num_neigh = "notch")) %>%
  group_by(nombre) %>% 
  ggplot(aes(x = minutes , y = NumberNeighbours, group = interaction(nombre, emx2_expression))) + 
  geom_line(aes(linetype = emx2_expression, col = invS), alpha = 1/3) + 
  geom_smooth(aes(group = interaction(emx2_expression, invS), linetype = emx2_expression, col = invS), n = 100, span = 0.3) +
  inversion_pos(time_of_last_Inv )

add_plot_style(p, yticks = 1, yleibel = "Number of Neighbours") + coord_cartesian(ylim = c(0,10)) + labs(ylab = "Number of Neighbours", title = "Neighbours of hair cells through time") +
  gghighlight::gghighlight() + 
  facet_wrap(vars(invS))


# Plotting coordinates ----------------------------------------------------
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , col = inv, dist_emx2_to_prog), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Distance of emx2 cell to progenitor position")

dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , col = inv, dist_notch_to_prog), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Distance of notch cell to progenitor position")

#Difference in distance to progenitor
dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , col = inv, dist_notch_to_prog-dist_emx2_to_prog), time_of_last_Inv ) + 
  labs(y = "Notch- Emx2 Microns", title = "Difference in distance to progenitor position")

dyn_net_df %>% filter(phenotype == "Wildtype") %>% 
  plot_comp_Inv(aes(x = minutes , col = inv, (dist_notch_to_prog+dist_emx2_to_prog)/2), time_of_last_Inv ) + 
  labs(y = "Notch + Emx2 Microns", title = "Mean distance to progenitor position")

dyn_net_df %>% filter(phenotype == "Wildtype") %>% mutate(dist_bw_hc = distance(emx2_x,notch_x, emx2_y, notch_y)) %>%
  plot_comp_Inv(aes(x = minutes , y = dist_bw_hc, col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Mean distance between hc siblings")

dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_x, col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Emx2 cell horizontal position")

dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = notch_x, col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Notch cell horizontal position")

dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = (notch_x - emx2_x), col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Difference in hair cell horizontal position")

dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = (notch_y - emx2_y), col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Difference in hair cell vertical position")

dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_y, col = inv), time_of_last_Inv ) + 
  labs(y = "Microns", title = "Emx2 cell vertical position")

# Plotting shape properties -----------------------------------------------
#Area emx2
p <- dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_area, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Microns", title = "Emx2 cell Area") +
  coord_cartesian(ylim = c(0,40))

p <- dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = notch_area, col = inv), time_of_last_Inv , yticks = 1000) + 
  labs(y = "Area (Pixels)", title = "Notch cell Area") +
  coord_cartesian(ylim = c(0,40))

#Mean Area 
p <- dyn_net_df %>% filter(phenotype == "Wildtype")  %>% mutate(mean_area = (notch_area + emx2_area) / 2) %>%
  plot_comp_Inv(aes(x = minutes , y = mean_area, col = inv), time_of_last_Inv , yticks = 1000) + 
  labs(y = "Area (Pixels)", title = "Mean cell area") +
  coord_cartesian(ylim = c(0,4000))

#Bond length
dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  plot_comp_Inv(aes(x = minutes , y = na_if(bond_length,0), col = inv), time_of_last_Inv , yticks = 1) + 
  labs(y = "Microns", title = "Bond length") #+
coord_cartesian(ylim = c(0,4000))

#Bond length by neuromast
p<-   dyn_net_df %>% filter(phenotype == "Wildtype", inv  == T)  %>%
  ggplot(aes(x = minutes , y = replace(bond_length,0,NA), col = nombre)) + 
  geom_line( alpha = 1/4) + 
  geom_smooth(n = 100, span = 0.3, alpha = 0.1 ) +
  inversion_pos(time_of_last_Inv)
add_plot_style(p,yleibel = "Microns") + 
  labs(y = "Microns", title = "Bond length", subtitle = "Inverting cells") 
#Area by emx2/notch And inversion
#Area by emx2/notch And inversion
p <- dyn_net_df %>% 
  select(minutes, emx2_area, notch_area, inv, nombre, time_of_last_Inv, invS) %>%
  gather(key = "hair_cell", value = "area", 2:3) %>% 
  mutate(hair_cell = recode(hair_cell, emx2_area = "emx2", notch_area = "notch")) %>%
  group_by(nombre) %>% 
  ggplot(aes(x = minutes , y = area, group = interaction(nombre, hair_cell))) + 
  geom_line(aes(linetype = hair_cell, col = invS), alpha = 1/3) + 
  geom_smooth(aes(group = interaction(hair_cell, invS), linetype = hair_cell, col = invS), n = 100, span = 0.3)

add_plot_style(p, yticks = 1000) + coord_cartesian(ylim = c(0,4000)) + labs(ylab = "Pixels", title = "Area of hair cells through time") +
  gghighlight::gghighlight() + 
  facet_wrap(vars(invS))
#perimeter by emx2 notch/ inversion /facet
p <- dyn_net_df %>% 
  select(minutes, emx2_perimeter, notch_perimeter, inv, nombre, time_of_last_Inv, invS) %>%
  #It is important that emx2_perimeter and notch_perimeter are the second and third selected
  #columns in the previous select
  gather(key = "emx2_expression", value = "Perimeter", 2:3) %>% 
  mutate(emx2_expression = recode(emx2_expression, emx2_perimeter = "emx2", notch_perimeter = "notch")) %>%
  group_by(nombre) %>% 
  ggplot(aes(x = minutes , y = Perimeter, group = interaction(nombre, emx2_expression))) + 
  geom_line(aes(linetype = emx2_expression, col = invS), alpha = 1/3) + 
  geom_smooth(aes(group = interaction(emx2_expression, invS), linetype = emx2_expression, col = invS), n = 100, span = 0.3)

add_plot_style(p, yticks = 10, yleibel = "Microns") + coord_cartesian(ylim = c(0,35))  +
  gghighlight::gghighlight() + 
  facet_wrap(vars(invS)) + labs(title = "Perimeter of hair cells through time")

#Circularity by emx2 notch/ inversion /facet
p <- dyn_net_df %>% 
  select(minutes, emx2_perimeter, notch_perimeter,emx2_area, notch_area, inv, nombre, time_of_last_Inv, invS) %>%
  transmute(minutes, emx2_circularity = circularity(emx2_area, emx2_perimeter), notch_circularity = circularity(notch_area, notch_perimeter), inv, nombre, time_of_last_Inv, invS) %>%
  #It is important that emx2_circularity and notch_circularity are the second and third selected
  #columns in the previous select
  gather(key = "emx2_expression", value = "Circularity", 2:3) %>% 
  mutate(emx2_expression = recode(emx2_expression, emx2_circularity = "emx2", notch_circularity = "notch")) %>%
  group_by(nombre) %>% 
  ggplot(aes(x = minutes , y = Circularity, group = interaction(nombre, emx2_expression))) + 
  geom_line(aes(linetype = emx2_expression, col = invS), alpha = 1/3) + 
  geom_smooth(aes(group = interaction(emx2_expression, invS), linetype = emx2_expression, col = invS), n = 100, span = 0.3)

add_plot_style(p, yticks = 0.1, yleibel = "Circularity") + coord_cartesian(ylim = c(0.3,0.85))  +
  gghighlight::gghighlight() + 
  facet_wrap(vars(invS)) + labs(title = "Circularity")

#Circularity of the whole pair
dyn_net_df %>% filter(phenotype == "Wildtype")   %>%
  mutate(Circularity = if_else(frame < 0, circularity(emx2_area, emx2_perimeter), circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length)))) %>%
  plot_comp_Inv(aes(x = minutes , y = Circularity, col = inv), time_of_last_Inv , yticks = 0.1) + 
  labs(y = "Circularity", title = "Hair cell pair Total Circularity") + 
  #227 is the minimum time in minutes that hair cells were tracked
  scale_x_continuous(expand = c(0,0), limits = c(0,227))
#Just inverting cells
p<- dyn_net_df %>% filter(phenotype == "Wildtype", inv == T)  %>% 
  mutate(Circularity = if_else(frame < 0, circularity(emx2_area, emx2_perimeter), circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length)))) %>%
  ggplot(aes(x = minutes , y = Circularity, col = nombre)) + 
  geom_line(alpha = 1/4) + 
  geom_smooth(n = 100, span = 0.3, alpha = 0.2, se = FALSE) +
  inversion_pos(time_of_last_Inv)
add_plot_style(p,yleibel = "Circularity", yticks = 0.1) + 
  labs(title = "Circularity of the haircell as a pair", subtitle = "Inverting cells")

#Comparing the maximum circularity with the height of inversion
p<- dyn_net_df %>% filter(phenotype == "Wildtype", inv == T, minutes >= 0)  %>% 
  mutate(Circularity = circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length))) %>% group_by(nombre) %>% slice_max(Circularity) %>% ggplot()
p <- p + theme_bw(base_size = 20) + geom_boxplot(aes(invS, time_of_last_Inv - minutes)) + geom_dotplot(aes(invS,time_of_last_Inv - minutes), binaxis = "y", stackdir = "center") + labs(x = "", color = '', y = "Time of inversion - time of max circ")

p<-dyn_net_df %>% filter(phenotype == "Wildtype", inv == T, minutes >= 0)  %>% 
  mutate(Circularity = circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length))) %>% group_by(nombre) %>% mutate(maxcirc = max(Circularity,na.rm = T)) %>% filter(minutes == time_of_last_Inv) %>%  ggplot()
p <- p + theme_bw(base_size = 20) + geom_boxplot(aes(invS, Circularity/maxcirc)) + labs(x = "", color = '', y = "circularity at inversion height/max(circularity)") +
  geom_dotplot(aes(invS,Circularity/maxcirc), binaxis = "y", stackdir = "center") + scale_y_continuous(limits = c(0,1.01))


#Difference in perimeter notch - emx2
dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  mutate(diffPerimeter = replace_na(notch_perimeter,0) - replace_na(emx2_perimeter,0)) %>%
  plot_comp_Inv(aes(x = minutes , y = diffPerimeter, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Microns", title = "Difference in perimeter", subtitle = "Emx2- vs Emx2+")

#Cummulative sum Difference in perimeter notch - emx2
dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  mutate(diffPerimeter = replace_na(notch_perimeter,0) - replace_na(emx2_perimeter,0)) %>%
  group_by(nombre)  %>% 
  mutate(cumdifPerimeter = cumsum(diffPerimeter)) %>%
  plot_comp_Inv(aes(x = minutes , y = cumdifPerimeter, col = inv), time_of_last_Inv , yticks = 50) + 
  labs(y = "Microns", title = "Cummulative sum of Difference in perimeter", subtitle = "Emx2- vs Emx2+")

#absolute of the Cummulative sum Difference in perimeter notch - emx2
dyn_net_df %>% filter(phenotype == "Wildtype")  %>%
  mutate(diffPerimeter = replace_na(notch_perimeter,0) - replace_na(emx2_perimeter,0)) %>%
  group_by(nombre)  %>% 
  mutate(cumdifPerimeter = abs(cumsum(sign(diffPerimeter)))) %>%
  plot_comp_Inv(aes(x = minutes , y = cumdifPerimeter, col = inv), time_of_last_Inv , yticks = 5) + 
  labs(y = "Microns", title = "Absolute Cummulative sum of sign Difference in perimeter", subtitle = "Emx2- vs Emx2+")



# Progenitor Properties ---------------------------------------------------
#Area of the progenitor
dyn_net_df %>% filter(phenotype == "Wildtype", minutes < 0)  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_area, col = inv), time_of_last_Inv , yticks = 1000) + 
  labs(y = "Area (Pixels)", title = "Progenitor area") + scale_x_continuous(expand = c(0,0), limits = c(-40,-1))

dyn_net_df %>% filter(phenotype == "Wildtype", minutes < 0, minutes > -12)  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_perimeter, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Microns", title = "Progenitor perimeter") + scale_x_continuous(expand = c(0,0), limits = c(-40,-1))

#Circularity of progenitor
dyn_net_df %>% filter(phenotype == "Wildtype", frame<0, frame > -12) %>%
  mutate(Circularity = circularity(emx2_area, emx2_perimeter)) %>%
  plot_comp_Inv(aes(x = minutes , y = Circularity, col = inv), time_of_last_Inv , yticks = 0.1) + 
  labs(y = "Circularity", title = "Progenitor Circularity") + scale_x_continuous(expand = c(0,0), limits = c(-40,0))


#Progenitor number of neighbours
dyn_net_df %>% filter(phenotype == "Wildtype", minutes < 0, minutes > -60)  %>%
  plot_comp_Inv(aes(x = minutes , y = emx2_Num_neigh, col = inv), time_of_last_Inv , yticks = 1) + 
  labs(y = "# Neighbours", title = "Progenitor Number of Neighbours") + scale_x_continuous(expand = c(0,0), limits = c(-40,-1))

#Progenitor properties 30 to 3 minutes before division
progenitor_df <- dyn_net_df %>% filter(phenotype == "Wildtype", minutes < 0, minutes > -10)  %>% group_by(nombre) %>% transmute(nombre, phenotype, inv, mean_area = mean(emx2_area), mean_neigh = mean(emx2_Num_neigh), perimeter = mean(notch_perimeter), invS) %>% unique

#Get the number of crossings in the neighbour difference
dyn_net_df %>% filter(phenotype == "Wildtype") %>% group_by(nombre) %>% mutate(changeofNeigh = crossing_as_ones(diff_N_neigh, mean(division), length(division))) %>% 
  mutate(cumChange = cumsum(changeofNeigh)) %>% 
  plot_comp_Inv(aes(x = minutes , y = cumChange, col = inv), time_of_last_Inv , yticks = 10) + 
  labs(y = "Y axis crossing", title = "Number of changes in neighbour dominance", subtitle = "How many times the difference of neighbours changes sign") + coord_cartesian(xlim = c(0,150))

#Plotting the mean instead of loess
mean_data <- dyn_net_df %>% filter(phenotype == "Wildtype") %>%
mutate(Circularity = if_else(frame < 0, circularity(emx2_area, emx2_perimeter), circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length)))) %>% 
  group_by(inv, minutes) %>% 
  summarise(cir = median(Circularity, na.rm = TRUE))
p<-ggplot()
  

# Circularity dataframe creation ------------------------------------------
#adding circularity data
dyn_net_df_circ <- dyn_net_df %>% filter(phenotype == "Wildtype") %>%
  mutate(Circularity = if_else(frame < 0, circularity(emx2_area, emx2_perimeter), circularity(emx2_area+notch_area, emx2_perimeter + notch_perimeter - (2 * bond_length))))
p + geom_line(data = dyn_net_df_circ, aes(x = minutes , y = Circularity, col = inv, group = nombre), alpha = 0.3) +
  geom_smooth(data = dyn_net_df_circ, aes(x = minutes , y = Circularity, col = inv, group = inv), n = 100, span = 0.3, size = 0, linetype = 2, level = 0.99) +
  geom_line(data = na.omit(mean_data), aes(x = minutes, y  = cir, colour = inv),  linetype = 1, size = 1) #+


#Reproducing Kai original figures of circularity and interface length with new data and new segmentation techniques

# Reproducing kai original circularity figures ----------------------------
#a dataframe with only one circularity column, the circularity of the progenitor, or, after division, the circularity of the pair as a whole
progenitorAndWhole <- transmute(dyn_net_df_circ, nombre = nombre, minutes = minutes, Circularity = Circularity, Cell = if_else(minutes < 0, "Prog", "Union"), invS)

#this dataframe has the circularity by notch emx2 cell
cellpair        <- filter(dyn_net_df_circ, minutes >= 0) %>%
  transmute(nombre = nombre, minutes = minutes, Cell1 = circularity(emx2_area, emx2_perimeter), Cell2 = circularity(notch_area, notch_perimeter), invS) %>%
  pivot_longer(starts_with("Cell"), names_to = "Cell", values_to = "Circularity")
cellpair$nombre <- factor(cellpair$nombre)

#bind both previous dfs by row. How to plot a single experiment
circularity_df  <- rbind(progenitorAndWhole,cellpair)

#how to plot circularity for all experiments
nombres <- circularity_df$nombre %>% unique
for(i in seq_along(nombres)){
  #take one experiment at a time "by_name" is "df"
  sp <- filter(circularity_df, nombre == nombres[i])
  
  g <- sp %>% ggplot(aes(x = minutes, y = Circularity, col = Cell, group = Cell)) +
    geom_point() + 
    geom_smooth(aes(x = minutes, y = Circularity, group= Cell), n = 50, span= 0.2, se = FALSE) + 
    scale_color_manual(breaks = c("Cell1", "Cell2", "Prog", "Union"), values=c("red", "green", "cyan", "blue")) + theme_bw() + ggtitle(nombres[i])
  #print just shows the plot, you can save here
  ggsave(filename = paste0(nombres[i],"_circularity.pdf"),g, device = "pdf", width=9, height=6, units = "in", useDingbats=F)
  #sleep two seconds
  Sys.sleep(2)}

#hardcoded start and end of inversions from fitting logistic function, in file fit_by_experiment.csv
circularity_df$StartInv <- NA
circularity_df$EndInv <- NA

circularity_df <- mutate(circularity_df, StartInv = case_when(
  nombre == "200504-wt-s1" ~ 115.20877225786009,         #200504_wt_s1
  nombre == "200504-wt-s2" ~ 181.05834,                  #200504_wt_s2
  nombre == "200723-wt-s2-t228-Inv" ~ 91.00724043403925, #200723_wt_s2_pair228
  nombre == "200723-wt-s4-t130-Inv" ~ 42.12089444123963, #200723_wt_s4_130
  nombre == "191108-wt-s2" ~ 135.417,         #191108_WT_s2
  nombre == "200724-wt-s4-t252-Inv" ~ 146.70371265536025, #200724_wt_s4_252
  nombre == "200807-s4-wt-t172-Inv" ~ 79.81112562998544,  #200807_wt_s4_pair172
  nombre == "200807-wt-s3-t254-Inv" ~ 110.04779497555639,  #200807_wt_s3_pair254
  nombre == "200724-s2-wt-t195-Inv" ~ 110.91268750326887,   #200724_wt_s2_pair195
  TRUE ~ NA_real_
),
EndInv = case_when(
  nombre == "200504-wt-s1" ~ 160.51262555114394,
  nombre == "200504-wt-s2" ~ 196.93005,
  nombre == "200723-wt-s2-t228-Inv" ~ 105.70516117891002,
  nombre == "200723-wt-s4-t130-Inv" ~ 105.2718778600972,
  nombre == "191108-wt-s2" ~ 194.7639,
  nombre == "200724-wt-s4-t252-Inv" ~ 159.33169848961646,
  nombre == "200807-s4-wt-t172-Inv" ~ 103.18279293201533,
  nombre == "200807-wt-s3-t254-Inv" ~ 124.73717244953743,
  nombre == "200724-s2-wt-t195-Inv" ~ 119.73326756206083,
  TRUE ~ NA_real_
)
)

#circularity_df <- na.omit(circularity_df)
circularity_df <- circularity_df %>% mutate(Stage = if_else(invS == "Inverting", case_when(
  minutes < StartInv ~ "Before",
  minutes > EndInv ~ "After",
  TRUE ~ "During"
),
#else, if they dont invert, we take everything as "before" inversion
"Before")
) 

circularity_by_stage <- group_by(circularity_df, nombre, Stage, Cell) %>% 
  summarise(Circularity = mean(Circularity, na.rm = T), invS = unique(invS))
###Supplementary figure 1
circularity_by_stage %>% filter(invS == "Inverting") %>% arrange(Stage) %>%
  #just arranging the order for the boxplot
  mutate(Stage = factor(Stage, levels = c("Before", "During", "After"))) %>% 
  filter(Cell != "Prog") %>%
  ggplot(aes(x = factor(Stage), y = Circularity, fill = factor(Cell))) + 
  #hide outliers because we are overlaying all the points anyway
  geom_boxplot(outlier.shape = NA, alpha = 0.5) +
  geom_dotplot(binaxis = "y", stackdir = "center", position = "dodge") + theme_bw() + scale_fill_manual(values=cbp2)

# Figure 2F Circularity ---------------------------------------------------
####Figure 2 F
circularity_by_stage %>%  filter(Cell == "Union") %>% arrange(Stage) %>%
  #just arranging the order for the boxplot
  mutate(Stage = factor(Stage, levels = c("Before", "During", "After"))) %>% 
  filter(Cell != "Prog") %>%
  ggplot(aes(x = factor(Cell), y = Circularity, fill = factor(Stage))) + 
  #hide outliers because we are overlaying all the points anyway
  geom_boxplot(outlier.shape = NA, alpha = 0.5) +
  geom_dotplot(binaxis = "y", stackdir = "center", position = "dodge") + facet_wrap (~invS) + theme_bw(base_size = 20) + scale_fill_manual(values=cbp2)


##wilcox test by stage During vs before
circularity_by_stage %>% filter(invS == "Inverting", Cell == "Union") %>% 
  select(nombre,Stage, Circularity) %>% pivot_wider(names_from = "Stage", values_from = "Circularity") %$%
  wilcox.test(During, Before)

##wilcox test by stage Before vs After
circularity_by_stage %>% filter(invS == "Inverting", Cell == "Union") %>% 
  select(nombre,Stage, Circularity) %>% pivot_wider(names_from = "Stage", values_from = "Circularity") %$%
  wilcox.test(After, Before)

##wilcox test by stage During vs After
circularity_by_stage %>% filter(invS == "Inverting", Cell == "Union") %>% 
  select(nombre,Stage, Circularity) %>% pivot_wider(names_from = "Stage", values_from = "Circularity") %$%
  wilcox.test(After, During)

##wilcox test by cell
circularity_by_stage %>% filter(invS == "Inverting", Stage == "Before") %>% 
  select(nombre,Cell, Circularity) %>% pivot_wider(names_from = "Cell", values_from = "Circularity") %$%
  wilcox.test(Cell2, Cell1)

# Summing angles for the cumulative angle plots ---------------------------
deg.rad <- function(x){x*pi/180}
rad.deg <- function(x){x*180/pi}
#sin of a+b
sin2<-function(a,b){sin(a)*cos(b) + sin(b)*cos(a)}
#cos of a+b
cos2<-function(a,b){cos(a)*cos(b) - sin(a)*sin(b)}
#Gives the rotation needed to go from angleb to anglea, where positive is a counterclockwise rotation
angledif <- function(anglea, angleb){
  #to sum (and substract) angles without making endless ifs for the two pi transitions, just sum the sines, sum the cosines, and take the atan2
  atan2(sin2(deg.rad(anglea),-deg.rad(angleb)),cos2(deg.rad(anglea),-deg.rad(angleb))) * 180 / pi}
###ditch the data before division because Int = Interface or bond between the siblings, progenitor does not have bond length
angleAndInt_df <- dyn_net_df_circ %>% filter(frame >= 0) %>% 
  select(nombre, minutes, emx2_x, emx2_y, notch_x, notch_y, bond_length, invS) %>%
  mutate(angle = atan2(emx2_y - notch_y, emx2_x - notch_x), angle_deg = rad.deg(angle)) %>% na.omit
#make a column with the angle, shifter by one timepoint 
angleAndInt_df <- angleAndInt_df %>% group_by(nombre) %>% mutate(pastAngle = lag(angle_deg)) %>%
  #take the differences and sum them to get the cumulative angle
  #na replacing by zero so the cummulative sum starts at 0
  mutate(d.phi.deg = replace_na(angledif(pastAngle, angle_deg), 0), cum.phi.deg = cumsum(d.phi.deg))


#join with circularity df
circularityUnion <- filter(circularity_df, Cell == "Union")
circularity_bond_df <- inner_join(circularityUnion, angleAndInt_df, by = c("nombre", "minutes", "invS"))



for(i in seq_along(nombres)){
  #take one experiment at a time "by_name" is "df"
  sp <- filter(circularity_bond_df, nombre == nombres[i])
  phase12 <- unique(sp$StartInv)
  phase23 <- unique(sp$EndInv)
  cumangle_plot <- sp %>% ggplot(aes(x = minutes, y = cum.phi.deg)) +
    geom_line() + theme_classic() + ggtitle(nombres[i]) +
    geom_vline(xintercept = c(phase12,phase23), color = "darkgrey", linetype = "dashed", size = 0.5)
  blength_Plot <- sp %>% ggplot(aes(x = minutes, y = bond_length)) + 
    geom_line() + theme_classic() + ggtitle(nombres[i])
    #geom_smooth(aes(x = minutes, y = Circularity, group= Cell), n = 50, span= 0.2, se = FALSE) + 
  #print just shows the plot, you can save here
  print(cumangle_plot)
  #ggsave(filename = paste0(nombres[i],"_bond_length.pdf"),
     #    plot_grid(cumangle_plot, blength_Plot,  nrow = 2, ncol = 1, align = "v", axis = "lr"),
     #    device = "pdf", width=9, height=15, units = "in", useDingbats=F)
}


# Bond length vs circularity for supplementary figure ---------------------

#circularity vs bond length
circvsbondlength <- circularity_bond_df %>% filter(invS == "Inverting") %>%ggplot(aes(x = bond_length, y = Circularity, colour = Stage)) + 
  geom_point(size = 2) + geom_smooth(aes(x = bond_length, y = Circularity), method = "lm", se = FALSE) + 
  theme_classic(base_size = 20) + scale_colour_manual(values=cbp2)
ggsave(filename ="circularityVSbondlength_supp2C.pdf",
       circvsbondlength,
       device = "pdf", width=13, height=8, units = "in", useDingbats=F)

#Spearman correlation calculations
inverting <- circularity_bond_df %>% filter(invS == "Inverting")
cor(inverting$bond_length, inverting$Circularity, method = "spearman")
inverting <- circularity_bond_df %>% filter(invS == "Inverting", Stage == "During")
cor(inverting$bond_length, inverting$Circularity, method = "spearman")
inverting <- circularity_bond_df %>% filter(invS == "Inverting", Stage == "Before")
cor(inverting$bond_length, inverting$Circularity, method = "spearman")
inverting <- circularity_bond_df %>% filter(invS == "Inverting", Stage == "After")
cor(inverting$bond_length, inverting$Circularity, method = "spearman")



# Side by side individual comparisons of circularity, bond length  --------
circularityUnion <- filter(circularity_df, nombre == "200504-wt-s2") %>% 
filter(Cell == "Union") 
  circularityUnionPlot <- circularityUnion %>% ggplot(aes(x = minutes, y = Circularity, col = Cell)) +
  geom_point() + 
  geom_smooth(aes(x = minutes, y = Circularity, group= Cell), n = 50, span= 0.2, se = FALSE) +
  geom_vline(xintercept = c(181.05834,196.93005), linetype = "dashed", size = 0.5) + theme_bw()
sp <- filter(angleAndInt_df, nombre == "200504-wt-s2")
cumangle_plot <- sp %>% ggplot(aes(x = minutes, y = cum.phi.deg)) +
  geom_line() + theme_classic() + theme(text = element_text(size=20)) + labs(x = "Minutes", y = "Cumulative angle") +
  geom_vline(xintercept = c(181.05834,196.93005), linetype = "dashed", size = 0.5)
blength_Plot <- sp %>% ggplot(aes(x = minutes, y = bond_length)) + labs(x = "Minutes", y = "Bond length [Microns]") +
  geom_line() + theme_classic() + theme(text = element_text(size=20)) +
  geom_vline(xintercept = c(181.05834,196.93005), linetype = "dashed", size = 0.5)

#### Three diferent examples for figure 2 of the paper
circularityUnion <- filter(circularity_df, nombre == "200807-wt-s3-t254-Inv") %>% 
  filter(Cell == "Union") %>% 
  ggplot(aes(x = minutes, y = Circularity, col = Cell)) +
  geom_point() + 
  
  geom_smooth(aes(x = minutes, y = Circularity, group= Cell), n = 50, span= 0.2, se = FALSE) +
  geom_vline(xintercept = c(110.04779644935269,124.73716890936163), linetype = "dashed", size = 0.5) + theme_bw()
sp <- filter(angleAndInt_df, nombre == "200807-wt-s3-t254-Inv")
cumangle_plot <- sp %>% ggplot(aes(x = minutes, y = cum.phi.deg)) +
  geom_line() + theme_classic() + theme(text = element_text(size=20)) + labs(x = "Minutes", y = "Cumulative angle") +
  geom_vline(xintercept = c(110.04779644935269,124.73716890936163), linetype = "dashed", size = 0.5)
blength_Plot <- sp %>% ggplot(aes(x = minutes, y = bond_length)) + labs(x = "Minutes", y = "Bond length [Microns]") +
  geom_line() + theme_classic() + theme(text = element_text(size=20)) +
  geom_vline(xintercept = c(110.04779644935269,124.73716890936163), linetype = "dashed", size = 0.5)
ggsave(filename ="USETHISONE-200807_wt_s3_pair254-Figure2C_D.pdf",
       plot_grid(circularityUnion, cumangle_plot, blength_Plot,  nrow = 3, ncol = 1, align = "v", axis = "lr"),
       device = "pdf", width=9, height=15, units = "in", useDingbats=F)



# Figure 2 C,DE -----------------------------------------------------------
##boundaries hardcoded from fitting logistic to cumulative angle
circularityUnion <- filter(circularity_df, nombre == "200807-s4-wt-t172-Inv") %>% 
  filter(Cell == "Union") %>% 
  ggplot(aes(x = minutes, y = Circularity)) + labs(x = "Minutes", y = "Circularity") +
  geom_point() + 
  geom_smooth(aes(x = minutes, y = Circularity, group= Cell), colour = "#B400FF", n = 50, span= 0.2, se = FALSE) +
  geom_vline(xintercept = c(79.81112652693795,103.18279257203969), linetype = "dashed", size = 1, color = c("green", "blue")) + theme_classic()  + theme(text = element_text(size=30))
sp <- filter(angleAndInt_df, nombre == "200807-s4-wt-t172-Inv")
cumangle_plot <- sp %>% ggplot(aes(x = minutes, y = cum.phi.deg)) +
  geom_line() + theme_classic() + theme(text = element_text(size=30)) + labs(x = "Minutes", y = "Cumulative angle [deg]") +
  geom_vline(xintercept = c(79.81112652693795,103.18279257203969), linetype = "dashed", size = 1, color = c("green", "blue"))
blength_Plot <- sp %>% ggplot(aes(x = minutes, y = bond_length)) + labs(x = "Minutes", y = "Bond length [Microns]") +
  geom_point() + 
  geom_smooth(aes(x = minutes, y = bond_length), colour = "red", n = 50, span= 0.2, se = FALSE) + 
  theme_classic() + theme(text = element_text(size=30)) + 
  geom_vline(xintercept = c(79.81112652693795,103.18279257203969), linetype = "dashed", size = 1, color = c("green", "blue"))
ggsave(filename ="USETHISONE-200807-s4-wt-t172-Inv-Figure2C_D.pdf",
       plot_grid(circularityUnion, cumangle_plot, blength_Plot,  nrow = 3, ncol = 1, align = "v", axis = "lr"),
       device = "pdf", width=9, height=15, units = "in", useDingbats=F)
