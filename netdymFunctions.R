library(network)
library(networkDynamic)
library(dplyr)
library('ndtv')
#Loads data and returns a dynamic network object
#Parameters: a directory, a file suffix (the part that the cell and bond parts have in common), the maximum area a cell is allowed to have (it will be resized),
#filtroborder [0,1,2] how many cells from the border should be trimmed for the network
loadCellAndBonds <- function(dir,filesufix, maxArea, filtroBorder, Proge, Sibling){
#import the info about cells including a persistent id through time, area, centroid coordinates, etc
celldata <- read_delim(paste0(dir,"cell", filesufix), 
                       "\t", escape_double = FALSE, trim_ws = TRUE)
celldata$filename <- NULL
print(names(celldata))
#max of the vertical side of the movie to flip and
#to render correctly as it is exported with 0 up
ysize <- celldata$center_y_cells %>% max(na.rm = TRUE) %>% ceiling
#an arbitrary area threshold to get rid of border cells
maxarea <- maxArea
#transform the data coordinate
celldata<- celldata %>% mutate(
  center_y_cells = ysize - center_y_cells,
  area_cells     = if_else(area_cells > maxarea, 580, area_cells)
)

celldatabyframe<-split(celldata, celldata$frame_nb)


##Import the info about which cells share a bond
bondsDf <- read_delim(paste0(dir,"bon", filesufix), delim = "\t", 
                      escape_double = FALSE, col_types = cols(cell_id_around_bond1 = col_double(), 
                                                              cell_id_around_bond2 = col_double(),
                                                              filename = col_skip()), na = "empty",
                      trim_ws = TRUE)

#A check. Must be true
typeof(bondsDf$cell_id_around_bond1) == "double"

names(bondsDf)

#just in case, otherwise the as.network function will fail
#But if the file was imported wrong and there is a blank column
#it will delete all rows
bondsDf<-na.omit(bondsDf)


#split edgelist by frame
b<-split(bondsDf, bondsDf$frame_nb)
#last frame, 0 based
fin<-length(b)-1

#Create a list of networks with edge and cell attributes
#The crucial thing here is that bondsDf was imported so that the first column is frame number
#and the 2nd and 3d columns are the cell nodes that form an edge 
#otherwise you have to change b[[i]][-1] so that the first two columns are the nodes of the edge
#Any remaining column can be imported as edge attributes
netlist <- list()
for (i in seq_len(length(b))){
  netlist[[i]] <- network(b[[i]][-1],  vertex.attr=celldatabyframe[[i]][-1], matrix.type="edgelist", 
                          loops=T, multiple=F, directed = F, ignore.eval = F)
  #We create an attribute called vertex.pid identifying the cells through time
  netlist[[i]] %v% "vertex.pid" <- netlist[[i]] %v% "track_id_cells"
}

#use only one of these two depending on your needs,
#as border cells are also border cells plus one

netlist_noborder <- netlist
if(filtroBorder == 1){
netlist_noborder<-lapply(netlist, function(redobject){
  isborder<-redobject %v% "is_border_cell"
  return(redobject %s% which(isborder == FALSE))
  })
}
if(filtroBorder == 2){
  netlist_noborder<-lapply(netlist, function(redobject){
    isborder<-redobject %v% "is_border_cell_plus_one"
    return(redobject %s% which(isborder == FALSE))
  })}


#using vertex.pid, which is the trackid that tissueanalyzer assigns the cells is crucial here
tnet<-networkDynamic(network.list=netlist_noborder, vertex.pid = "vertex.pid", create.TEAs = TRUE)

#make all vertex black. 
activate.vertex.attribute(tnet,'color',rgb(0,0,0),onset=-Inf,terminus=Inf)

#the ids are determined from tissue analyzer
trID_prog <- Proge
trID_sib <-  Sibling 
#The id of the progenitor given by network dynamic
pid <- get.vertex.id(tnet, trID_prog)
#The id of the sibling cell
sid <- get.vertex.id(tnet,trID_sib)

neigh_prog_any <- get.neighborhood.active(tnet, pid, onset = 0, terminus = fin, rule = "any")
neigh_sib_any <- get.neighborhood.active(tnet, sid, onset = 0, terminus = fin, rule = "any")
subn_trackIDs<- sapply(union(neigh_prog_any,neigh_sib_any), function(x){get.vertex.pid(tnet,x)})

#get a list of networks with only the hair cells and their neighbours
onlyHcsandNeighList <- lapply(seq_len(fin+1), function(i){
  #vector of trackids for the given network
  a<-netlist_noborder[[i]] %v% "track_id_cells"
  #the indexes ids of the hair cells given their trackids
  ind<-which(a %in% subn_trackIDs)
  netlist_noborder[[i]] %s% ind})

tnet <- coloreaVertices(tnet, pid, sid, fin)

  
tnetsub<-networkDynamic(network.list=onlyHcsandNeighList, vertex.pid = "vertex.pid", create.TEAs = TRUE)
#make all vertex black. 
activate.vertex.attribute(tnetsub,'color',rgb(0,0,0),onset=-Inf,terminus=Inf)
#The id of the progenitor in the new subnetwork
pid <- get.vertex.id(tnetsub, trID_prog)
#The id of the sibling cell
sid <- get.vertex.id(tnetsub,trID_sib)
tnetsub <- coloreaVertices(tnetsub, pid, sid, fin)
return(list(main = tnet, subnet = tnetsub))
}

#Graph the temporal networks with colors and all. Choose the output type and file
bigGraph <- function(temporalNetwork, archivo, output){
  #compute the animation using the actual coordinates of the cell through time 
  #that we imported and passed as a TEA
  compute.animation(temporalNetwork, animation.mode='useAttribute', layout.par = list(x = "center_x_cells", y = "center_y_cells"))
  
  #render net animation as html. Scale node size in relationship to actual cell area
  render.d3movie(temporalNetwork, output.mode = output, vertex.col = "color", filename = paste0(archivo,".html"), vertex.cex =function(slice){(slice %v% "area_cells")/580}, label= temporalNetwork %v% "vertex.pid", displaylabels=F,   vertex.tooltip = function(slice) {
    paste(
      "<b>Track:</b>", (slice %v% "vertex.pid"),
      "<br>",
      "<b>Local:</b>", (slice %v% "local_id_cells"),
      "<br>",
      "<b>Name:</b>", (slice %v% "vertex.names")
    )
  })
}

#Color network. Given two node ids, colour them and their neighbours in the network
#In a manner defined by whether the neighbours touch one, or the other or both of the original
coloreaVertices <- function(net2col, node1, node2, termino){
#get the ids of the cells that are touching the progenitor, this includes the other hair cell
neigh_prog <- lapply(0:termino,function(time){get.neighborhood.active(net2col, v = node1, at = time)})
#get the id of the other hair cell, one of the sibling keeps the persistent id of the progenitor
neigh_sib  <- lapply(0:termino,function(time){get.neighborhood.active(net2col, v = node2, at = time)})

#get the cells that are touching both hair cells at a given time
shared     <-mapply(intersect, neigh_prog, neigh_sib)

#make all vertex black. Use a color pallete adapted for color blindness
activate.vertex.attribute(net2col,'color',rgb(0,0,0),onset=-Inf,terminus=Inf)
for(i in 0:termino){
  #sky blue the cells touching the progenitor
  activate.vertex.attribute(net2col,'color',rgb(86,180,233, maxColorValue = 255), onset = i, terminus = i+1, v = neigh_prog[[i+1]])
  #yellow the cells touching the sibling (the other hair cell)
  activate.vertex.attribute(net2col,'color',rgb(240,228,66, maxColorValue = 255), onset = i, terminus = i+1, v = neigh_sib[[i+1]])
  #bluish green the shared contacts
  activate.vertex.attribute(net2col,'color',rgb(0,158,115, maxColorValue = 255), onset = i, terminus = i+1, v = shared[[i+1]])
}

#the progenitor is blue
activate.vertex.attribute(net2col,'color',rgb(0,114,178, maxColorValue = 255), onset = -Inf, terminus = Inf, v = node1)
#The sibling is orange
activate.vertex.attribute(net2col,'color',rgb(230,159,0, maxColorValue = 255), onset = -Inf, terminus = Inf, v = node2)
return(net2col)
}

get.vertex.fromnameID <- function(nd, name){
  which(nd %v% "vertex.names" == name)
}

#get the last time point of a network
netLength <- function(nd){get.network.attribute(nd, "net.obs.period", unlist = TRUE)["observations2"] %>% as.numeric()}

circularity <- function (area, perimeter){
  return(4*pi*area/(perimeter^2))
}


########
#######
